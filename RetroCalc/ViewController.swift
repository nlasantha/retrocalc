//
//  ViewController.swift
//  RetroCalc
//
//  Created by Nuwan on 2016/10/05.
//  Copyright © 2016 Muse Pixel. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var btnSound : AVAudioPlayer!
    @IBOutlet weak var outputLabel: UILabel!

    
    enum Operation : String {
        case Divide = "/"
        case Multiply = "*"
        case Substract = "-"
        case Add = "+"
        case Empty = "Empty"
        
    }
    var currentOperator = Operation.Empty
    var runningNumber = ""
    var leftValStr = ""
    var rightValStr = ""
    var result  = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let path = Bundle.main.path(forResource: "btn", ofType: "wav")
        let soundURL = URL(fileURLWithPath: path!)
        do{
            try btnSound = AVAudioPlayer(contentsOf: soundURL)
            btnSound.prepareToPlay()
        }catch let err as NSError{
            print(err.debugDescription)
        }
        outputLabel.text = "0"
        
    }
    
    @IBAction func numberPressed(sender: UIButton){
        playSound()
        
        runningNumber += "\(sender.tag)"
        outputLabel.text = runningNumber
    }
    @IBAction func onDividePressed(sender: AnyObject){
        processOperation(operation: .Divide)
    }
    @IBAction func onMultiplyPressed(sender: AnyObject){
        processOperation(operation: .Multiply)
    }
    @IBAction func onSubstravtPressed(sender: AnyObject){
        processOperation(operation: .Substract)
        
    }
    @IBAction func onAdditionPressed(sender: AnyObject){
        processOperation(operation: .Add)
    }
    @IBAction func onEqualPressed(sender: AnyObject){
        processOperation(operation: currentOperator)
    }
    
    
    
    func playSound(){
        if btnSound.isPlaying{
            btnSound.stop()
        }
        btnSound.play()
    }
    

    func processOperation(operation : Operation){
        playSound()
        if currentOperator != Operation.Empty{
            
            //A user selected an operator, but then selected another operator first enetring a number
            if runningNumber != "" {
                rightValStr = runningNumber
                runningNumber = ""
                if currentOperator == Operation.Multiply{
                    result = "\(Double(leftValStr)! * Double(rightValStr)!)"
                }else if currentOperator == Operation.Divide{
                     result = "\(Double(leftValStr)! / Double(rightValStr)!)"
                }else if currentOperator == Operation.Substract{
                     result = "\(Double(leftValStr)! - Double(rightValStr)!)"
                }else if currentOperator == Operation.Add{
                     result = "\(Double(leftValStr)! + Double(rightValStr)!)"
                }
                
                leftValStr = result
                outputLabel.text = result
                
            }
            currentOperator = operation
        }else{
            //This is the first time and operator has been pressed
            leftValStr = runningNumber
            runningNumber = ""
            currentOperator = operation
        }
        
    }

   


}

